<?php

use yii\db\Migration;

/**
 * Class m180624_074850_init_rbac
 */
class m180624_074850_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
                $auth = Yii::$app->authManager;
        
                // add "author" role and give this role the "createPost" permission
                 $admin = $auth->createRole('admin');
                $auth->add($admin);

                $manager = $auth->createRole('manager');
                $auth->add($manager);

                $employee = $auth->createRole('employee');
                $auth->add($employee);

                $auth->addChild($admin, $manager);
                $auth->addChild($manager, $employee);


                $createTask = $auth->createPermission('createTask');//לעובד מותר ליצור משימות
                $auth->add($createTask);

                $viewTasks = $auth->createPermission('viewTasks');//לעובד מותר לצפות במשימות
                $auth->add($viewTasks);

                $viewUsers = $auth->createPermission('viewUsers');//לעובד מותר לצפות ברשימת משתמשים
                $auth->add($viewUsers);                
                
                $deleteUser = $auth->createPermission('deleteUser');//אף אחד לא מורשה למחוק משתמשים
                $auth->add($deleteUser); 

                $updateUsers = $auth->createPermission('updateUsers');//רק למנהל מותר לערוך משתמשים
                $auth->add($updateUsers);    

                $indexUser = $auth->createPermission('indexUser');
                $auth->add($indexUser);

                 $indexTask = $auth->createPermission('indexTask');
                 $auth->add($indexTask);

                 $updateTask = $auth->createPermission('updateTask');//רק למנהל מותר לערוך משימות
                $auth->add($updateTask);    

                $deleteTask = $auth->createPermission('deleteTask');//רק למנהל מותר למחוק משימות
                $auth->add($deleteTask);   
               ///// 
                $updateOwnUser = $auth->createPermission('updateOwnUser');//לעובד מותר לערוך את הפרופיל משתמש של עצמו בלבד
                $rule = new \app\rbac\UserRule;
                $auth->add($rule);
                
                
                $updateOwnUser->ruleName = $rule->name;                 
                $auth->add($updateOwnUser);                  
                //////



        $auth->addChild($admin, $deleteUser);
        $auth->addChild($employee, $createTask);
        $auth->addChild($employee, $viewTasks);
        $auth->addChild($employee, $indexUser);
        $auth->addChild($employee, $indexTask);
        $auth->addChild($employee, $viewUsers);
        $auth->addChild($employee, $updateOwnUser);
        $auth->addChild($manager, $updateUsers);
        $auth->addChild($manager, $updateTask);
        $auth->addChild($manager, $deleteTask);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180624_074850_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180624_074850_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
